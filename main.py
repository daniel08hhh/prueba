from cgi import test
from flask import Flask, jsonify, request
from flask_restful import Resource, Api
import requests
import json
#Conexion
from conexion import insertData, conexion, insertDataL,SearchID


app = Flask(__name__)
api = Api(app)


@app.route('/insertUser', methods=['GET'])
def stateSite():
    #recoleccion data
    data = json.loads(request.data)
    print(data)

    if 'nombre' not in data : 
        return jsonify({"ERROR" : "Parametro sede incompleto, revise sus datos"})
    if 'celular' not in data : 
        return jsonify({"ERROR" : "Parametro sede incompleto, revise sus datos"})
    
    requestNombre = data['nombre']
    requestCelular = data['celular']

    print(insertData(requestNombre, requestCelular))


    print(requestNombre)
    print(requestCelular)


    return "HOLA"

@app.route('/consulta', methods=['GET'])
def consulta_libro():
    #recoleccion data
    data = json.loads(request.data)
    print(data)

    if 'autor' not in data : 
        return jsonify({"ERROR" : "Parametro sede incompleto, revise sus datos"})

    
    requestAutor = data['autor']


    prueba=requests.get('https://www.googleapis.com/books/v1/volumes?q=soledad+inauthor:{}&key=AIzaSyD6K87HVdRdM_Jb3YpGqndXUKcS5o-U5A4'.format(requestAutor))
    
    test = prueba.json()

    array = test['items']
    title=(array[0]['volumeInfo']['title'])

    return jsonify({"titulo":title})

@app.route('/populares', methods=['GET'])
def consulta_libro_popular():
    #recoleccion data
    data = json.loads(request.data)
    print(data)

    if 'autor' not in data : 
        return jsonify({"ERROR" : "Parametro sede incompleto, revise sus datos"})

    
    requestAutor = data['autor']


    prueba=requests.get('https://www.googleapis.com/books/v1/volumes?q=soledad+inauthor:{}&orderBy=relevance&key=AIzaSyD6K87HVdRdM_Jb3YpGqndXUKcS5o-U5A4'.format(requestAutor))
    
    test = prueba.json()

    array = test['items']
    
    popular = []

    for x in array : 
        infotest = x['volumeInfo']['title']
        popular.append(infotest)

    print(popular)

    return jsonify({"Libros" :popular})

@app.route('/Libro', methods=['GET'])
def consulta_libro_deseado():
    #recoleccion data
    data = json.loads(request.data)
    #print(data)

    if 'libro' not in data : 
        return jsonify({"ERROR" : "Parametro sede incompleto, revise sus datos"})
    if 'nombre' not in data : 
        return jsonify({"ERROR" : "Parametro sede incompleto, revise sus datos"})

    
    requestLibro = data['libro']
    requestNombre = data['nombre']
    ID=(SearchID(requestNombre))


    prueba=requests.get('https://www.googleapis.com/books/v1/volumes?q={}+inauthor:Gabriel&orderBy=relevance&key=AIzaSyD6K87HVdRdM_Jb3YpGqndXUKcS5o-U5A4'.format(requestLibro))
    
    test = prueba.json()

    array = test['items']
    

    autor=(array[0]['volumeInfo']['authors'][0])
    fecha_publicacion=(array[0]['volumeInfo']['publishedDate'])
    titulo=(array[0]['volumeInfo']['title'])
    editorial = " "

    print(insertDataL(ID,editorial, autor, titulo, fecha_publicacion))

    return jsonify({"autor" : autor,
                    "fecha_publicacion":fecha_publicacion,
                    "titulo":titulo,
                    "editorial":""})


if __name__ == '__main__':
    app.run(debug=True)


