FROM ubuntu:focal

WORKDIR /usr/src/app
ENV DEBIAN_FRONTEND noninteractive
COPY . .
RUN apt-get update

RUN apt install  --no-install-recommends unixodbc-dev -y
RUN apt install libpq-dev -y

RUN pip3 install -r requirements.txt