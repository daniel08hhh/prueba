import psycopg2

#--------------------------------------------Conexion-------------------------------------------
def conexion() : 
    conn = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="colombia2015")
    cur = conn.cursor()
    cur.execute('SELECT * FROM "Cliente"')
    nombre = ''
    for nombre in cur.fetchall() :
        nombre = nombre
        return nombre
    conn.close()

#--------------------------------------------Insert-------------------------------------------
def insertData(name, celular ) : 
    conn = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="colombia2015")
    cur = conn.cursor()

    postgres_insert_query = ''' INSERT INTO "Cliente"("NOMBRE", "CELULAR") VALUES (%s,%s)'''
    record_to_insert = (name , celular)
    cur.execute(postgres_insert_query, record_to_insert)

    conn.commit()
    cur.close()
    return "DATOS INSERTADOS CORRECTOS"

def insertDataL(id,editorial, autor, nombre, fecha) : 
    conn = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="colombia2015")
    cur = conn.cursor()

    postgres_insert_query = ''' INSERT INTO "LIBRO"("ID","EDITORIAL", "AUTOR" , "NOMBRE_LIBRO", "FECHA") VALUES (%s,%s,%s,%s,%s)'''
    record_to_insert = (id, editorial, autor, nombre, fecha)
    cur.execute(postgres_insert_query, record_to_insert)

    conn.commit()
    cur.close()
    return "DATOS INSERTADOS CORRECTOS"

def SearchID(nombre) : 
    conn = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password="colombia2015")
    cur = conn.cursor()

    postgres_insert_query = 'SELECT "ID" FROM "Cliente" WHERE "NOMBRE" =%s'
    record_to_insert = (nombre)
    cur.execute(postgres_insert_query,(record_to_insert,))
    dataarray = cur.fetchall()
    print (dataarray)

    conn.commit()
    cur.close()
    return dataarray[0]   

